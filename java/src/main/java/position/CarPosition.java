package position;

public class CarPosition {
	public Id id;
	public double angle;
	public PiecePosition piecePosition;
	
	public CarPosition() {
		piecePosition = new PiecePosition();
		id = new Id();
		piecePosition.lane = new Lane();
	}
}
