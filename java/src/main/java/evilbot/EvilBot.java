// messy. didn't have time to clean it up. works better than previous versions
// This version saves maximum throttle values for each track piece. And it adjusts them
// depending on the angle and if the car crashes. Implementation is, as said, messy and
// not optimised to work well enough. It works altogether better than my previous versions.

// In comments we means I. (solo work)

package evilbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import messages.Join;
import messages.JoinRace;
import messages.Ping;
import messages.Response;
import messages.SendMsg;
import messages.SwitchLane;
import messages.Throttle;
import messages.YourCar;
import messages.Turbo;
import position.CarPosition;
import track.Lane;
import track.Piece;
import track.Position;
import track.Sector;
import track.StartingPoint;
import track.Track;
import car.Car;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class EvilBot {
	boolean log = true;
	
	// car controlling variables
	Car me;
	Track track;
	Map<String, Car> opponents;
	YourCar myCar;
	public boolean turboAvailable = false;
	boolean switchingLane = false;
	double[] maxThrottles;							// Maximum throttle values for each track piece
	double maxAngle = 45;							// crash angle is 60, but to be safer we set it lower
	int pieceAfterCrashCurve = -1;
	int longestStraight = -1;
	
	// message handling variables
	BufferedReader messageReceiver;
	PrintWriter messageSender; 
	String message;
	final Gson gson;
	
	int tick = 0;
	int previousTick = 0;
	
	public EvilBot(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey) throws IOException {
		this.messageReceiver = reader;
		this.messageSender = writer;
		
		this.gson = new Gson();
		
		this.me = new Car();
		this.opponents = new HashMap<String, Car>();
		
		me.throttle = 1.0;						// we start at full throttle;
		
//		JoinRace joinRace = new JoinRace(botName, botKey, "elaeintarha");
		Join joinRace = new Join(botName, botKey);
		
		send(joinRace);
		
	}
	
	public void run() throws IOException {
		// get message from server
		// depending on message, act upon it
		while((message = messageReceiver.readLine()) != null) {
			previousTick = tick;

            final Response msgFromServer = gson.fromJson(message, Response.class);
        	
            tick = msgFromServer.gameTick;
            
            if(turboAvailable && me.position.piecePosition.pieceIndex == longestStraight) {   //track.pieces.get((int) position.pieceIndex).angle == 0) {
            	send(new Turbo());
            	turboAvailable = false;
//            	System.out.println("Turbo engaged! @tick: " + tick + " @" + position.pieceIndex + "," + position.inPieceDistance);
            }
            
//            if(k == -1 && velocity > 0) {
//            	k = koefficient(velocity, throttle);
//            }
            
            if(tick > 1) {								// checking starts from tick 2 to make sure all class instances are created
            	// lane switching logic
	            if(!switchingLane) {
	            	switchLane();
	            }
	            
            	if(me.previousPosition.piecePosition.pieceIndex == track.switches.get(me.sector)
            			&& me.previousPosition.piecePosition.pieceIndex != me.position.piecePosition.pieceIndex) {
            		me.sector = track.nextSector(me.sector);
            		switchingLane = false;
            		System.out.println("Sector " + me.sector + " pIndex " + me.position.piecePosition.pieceIndex);
            	}
            	
            	// checking if piece was changed and calling throttle increase function
            	if(me.position.piecePosition.pieceIndex != me.previousPosition.piecePosition.pieceIndex)
            		increaseMaxThrottle(me.position.piecePosition.pieceIndex);
            	
            	// finding longest straight for using turbo
            	if(longestStraight == -1)
            		findLongestStraight();
            }
            
            
            if (msgFromServer.msgType.equals("carPositions")) {
            	parsePositions(msgFromServer.data);
            	
            	calculateVelocity(me);
            	
            	double brakeDistance = -1;
            	
            	if(me.k == -1 && me.velocity > 0) {							// we can calculate the coefficient of velocity  
            		me.koefficient(me.velocity, me.throttle);				// function at the very first velocity value we get
            	} else {					// if we have already value, we start calculating current brakeDistance
            		brakeDistance = brakingDistance(me.velocity, 10 * maxThrottles[nextCurve()]);	
            	}
            	
            	double distanceToCurve = distanceToNextCurve();
            	
            	me.throttle = maxThrottles[me.position.piecePosition.pieceIndex];
            	
            	if(distanceToCurve < brakeDistance && brakeDistance > 0 &&					// distance to curve is less than brake distance, we brake
            	   track.pieces.get(me.position.piecePosition.pieceIndex).radius == 0) {	// we check if we are still on straight.
                	me.throttle = 0;														// if in curve, we throttle at maximum throttle of that piece (set before this if-clause) 
                } 									// fixed a stupid bug here by checking if on straight
            										// before the fix it checked braking distance (and braked) on curves too
            	
            	send(new Throttle(me.throttle));
            	
            	
            } else if (msgFromServer.msgType.equals("yourCar")) {
            	myCar = gson.fromJson(gson.toJson(msgFromServer.data), YourCar.class);
            	
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                parseRace(msgFromServer.data);
                track.buildSectors();
                initMaxThrottles();
//                for(int i = 0;i < track.switches.size();i++)
//                	System.out.println(track.switches.get(i));
                
                                
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Throttle(me.throttle));
                
            } else if(msgFromServer.msgType.equals("crash")) {
            	YourCar crashedCar = gson.fromJson(gson.toJson(msgFromServer.data), YourCar.class);
            	if(crashedCar.color.equals(myCar.color)) {
            		decreaseMaxThrottle(me.position.piecePosition.pieceIndex);
            	}
            	System.out.println("Crash! (" + crashedCar.color + ", " + crashedCar.name + ")");
            	
            	pieceAfterCrashCurve = me.position.piecePosition.pieceIndex;
            	
            	while(true) {
            		if(track.pieces.get(pieceAfterCrashCurve).angle == 0) {
            			System.out.println("a");
            			break;
            		}
            		
            		double angle = track.pieces.get(pieceAfterCrashCurve).angle;
        			double nextAngle = track.pieces.get(nextPiece(pieceAfterCrashCurve)).angle;
        			if((angle < 0 && nextAngle > 0) || (angle > 0 && nextAngle < 0)) {
            			System.out.println("b");
        				break;
        			}
        			
        			pieceAfterCrashCurve++;
            	}
            	
            	System.out.println("next straight:" + pieceAfterCrashCurve);
            	
            } else if(msgFromServer.msgType.equals("turboAvailable")) {
//            	System.out.println("Turbo! " + position.pieceIndex + ", " + position.inPieceDistance + ", lap " + position.lap + " tick: " + tick);
            	turboAvailable = true;
            	
            } else {
                send(new Ping());
            }
            
            
		}
	}
	
	private void findLongestStraight() {
		int startIter = nextCurve();
		int nextCurve =  startIter+ 1;
		int straightStart;
		double longestSoFar = 0;
		
		int i = nextCurve;
		
		while(i != startIter) {
			i = nextPiece(i);

			if(track.pieces.get(i).angle == 0) {
				straightStart = i;
				int straightLength = 0;
				while(track.pieces.get(i).angle == 0) {
					straightLength += track.pieces.get(i).length;
					i = nextPiece(i);
				}
				
				if(straightLength > longestSoFar) {
					longestSoFar = straightLength;
					longestStraight = straightStart;
				}
			}
		}
		
		System.out.println("Longest straight start piece: " + longestStraight);
	}
	
	private void decreaseMaxThrottle(int piece) {	// called when I crashed. Here we adjust max throttle for whole curve where crash occurred
		int curveStarted = piece;
		int curveEnds = piece;
		
		double curveLength = 0;			// the length of whole curve (on current lane)
		double crashDistance = 0;		// calculate the crash position from the start of the curve
		
		if(Math.abs(track.pieces.get(piece).angle) < 0.1) {			// if crash happened just after the curve (happened once during testing and whole program crashed)
			maxThrottles[previousPiece(piece)] *= 0.95;				// previous curve pieces max throttle is decreased
			return;													// nothing else is done. and we hope next lap it doesn't crash at same point.
		}
		
		// we find out the start of the curve
		while(Math.abs(track.pieces.get(previousPiece(curveStarted)).radius - 
					   track.pieces.get(piece).radius) < 0.01) {
			
			double angle = track.pieces.get(piece).angle;
			double previousAngle = track.pieces.get(previousPiece(curveStarted)).angle;
			if((angle < 0 && previousAngle > 0) || (angle > 0 && previousAngle < 0))
				break;
				
			curveStarted = previousPiece(curveStarted);
		}
		
		// then the end of the curve
		while(Math.abs(track.pieces.get(nextPiece(curveEnds)).radius - 
				   track.pieces.get(piece).radius) < 0.01) {
			
			double angle = track.pieces.get(piece).angle;
			double nextAngle = track.pieces.get(nextPiece(curveStarted)).angle;
			if((angle < 0 && nextAngle > 0) || (angle > 0 && nextAngle < 0))
				break;
			
			curveEnds = nextPiece(curveEnds);
		}
		
		if(curveEnds == curveStarted) {					// in case crash piece is only one piece long, we have to adjust previous pieces too
			maxThrottles[curveEnds-1] *= 0.6;			// this has better solution, but for now it can be this
		}
		
		// calculating crash distance and curve length in same loop
		for(int i = curveStarted; i <= curveEnds; i = nextPiece(i)) {
			Piece trackPiece = track.pieces.get(i);
			Lane lane = track.lanes.get(me.position.piecePosition.lane.endLaneIndex);
			
			double laneLength = Math.PI * (trackPiece.radius - lane.distanceFromCenter * Sector.sign(trackPiece.angle)) * (Math.abs(trackPiece.angle) / 180); 
			
			curveLength += laneLength;
			
			if(i < piece) {							// if the crash point is not the first piece of the curve, we must add all the previous lengths of the curve pieces
				crashDistance += laneLength;
			}
		}
		
		crashDistance += me.position.piecePosition.inPieceDistance;		// the inPieceDistance is added after all previous lengths are added.
		
		double curveCovered = crashDistance / curveLength;
		
		System.out.println("crashed at piece " + piece);
		System.out.println("the first piece of curve: " + curveStarted);
		System.out.println("the last piece of curve: " + curveEnds);
		System.out.println("Curve length is " + curveLength);
		System.out.println("crash distance is " + crashDistance);
		System.out.println(100 * curveCovered + "% of the curve was covered");
		System.out.println("adjusting throttle values for current curve...");
		
		for(int i = curveStarted; i <= curveEnds; i = nextPiece(i)) {
			System.out.print("adjusting piece " + i + " maximum throttle value from " + maxThrottles[i]);
//			maxThrottles[i] *= curveCovered;										// this solution appears not to be very good, so making it
			maxThrottles[i] *= 0.9;													// decrease constant percentage
			System.out.println(" to " + maxThrottles[i]);
		}
	}
	
	private void increaseMaxThrottle(int piece) {
		if(pieceAfterCrashCurve != -1 && piece > pieceAfterCrashCurve)
			pieceAfterCrashCurve = -1;
		
		if(pieceAfterCrashCurve == -1) {
			if(track.pieces.get(piece).radius != track.pieces.get(previousPiece(piece)).radius &&
			   track.pieces.get(previousPiece(piece)).radius != 0) {	// if radius changes, we are out of the curve
				System.out.println("previous position angle: " + me.previousPosition.angle);
				
				double deltaAngle = maxAngle -  Math.abs(me.previousPosition.angle);
				double angleRatio = deltaAngle / maxAngle;
				
				System.out.println("angle ratio: " + angleRatio);
				
				int curveStarted = piece - 1;
				
				while(Math.abs(track.pieces.get(previousPiece(curveStarted)).angle - 
						   track.pieces.get(piece - 1).angle) < 0.01) {
					curveStarted--;
				}
				
				System.out.println("curve start " + curveStarted);
				
				for(int i = curveStarted; i < piece; i++) {
					System.out.print("throttle on piece " + i + " increased from " + maxThrottles[i]);
					maxThrottles[i] = Math.min(1.0, (1 + angleRatio / 10) * maxThrottles[i]);
					System.out.println(" to " + maxThrottles[i]);
				}
			}
		}
	}
	
	private int previousPiece(int index) {
		if(index == 0) {
			return track.pieces.size() - 1;
		}
		
		return index - 1;
	}
	
	private int nextPiece(int index) {
		if(index == track.pieces.size() - 1) {
			return 0;
		}
		
		return index + 1;
	}
	
	private void initMaxThrottles() {
		maxThrottles = new double[track.pieces.size()];
		for(int i = 0; i < maxThrottles.length; i++) {
			if(track.pieces.get(i).angle == 0)
				maxThrottles[i] = 1.0;
			else {
				maxThrottles[i] = Math.min(1.0, track.pieces.get(i).radius / (50*5));
			}
		}
	}
	
	private void switchLane() {
		switchingLane = true;
		int index = track.nextSector(me.sector);
		
		while(true) {
			if(track.shortestRoute[index] == -1) {
				index = track.nextSector(index);
				continue;
			}
			break;
		}

		System.out.println("index " + index);
		System.out.println("shortest: " + track.shortestRoute[index]);
		System.out.println("mylane: " + me.position.piecePosition.lane.endLaneIndex);
		
		if(track.shortestRoute[index] - me.position.piecePosition.lane.endLaneIndex < 0) {
			send(new SwitchLane("Left"));
			System.out.println("Switching left");
		} else if(track.shortestRoute[index] - me.position.piecePosition.lane.endLaneIndex > 0) {
			send(new SwitchLane("Right"));
			System.out.println("Switching right");
		} else {
			System.out.println("Not switching lane");
		}
	}
	
	//			method to determine the last point to start decelerating (throttle to 0)
	// 		when accelerating: velocity(t) = power * throttle * (1 - e^(-t * k)) 	k is constant
	//		and decelerating: velocity(t) = v * e^(-t * k)							v is velocity at start of deceleration
	//		by these functions we can calculate the distance to start deceleration
    private double brakingDistance(double currentVelocity, double targetVelocity) {
    	double t = (Math.log(currentVelocity) - Math.log(targetVelocity)) / (me.k);
    	
    	return (currentVelocity / me.k) * (1 - Math.exp(-t * me.k));
    }
    
    //			methods to help to calculate the point to start deceleration 
	private int nextCurve() {
    	int i = (int) me.position.piecePosition.pieceIndex;
    	while(track.pieces.get(i).radius == 0) {
    		i++;
    		if(i == track.pieces.size()) {
    			i = 0;
    		}
    	}
    	
    	return i;
    }
    
    private double distanceToNextCurve() {
    	int i = me.position.piecePosition.pieceIndex;

    	if(track.pieces.get(i).radius != 0) {
    		return 0;
    	} 
    	
    	double distance = 0;
    	
    	while(track.pieces.get(i).radius == 0) {
    		distance += track.pieces.get(i++).length;
    		if(i == track.pieces.size()) {
    			i = 0;
    		}
    	}
    		
    	return distance - me.position.piecePosition.inPieceDistance;
    }
	
	private void send(final SendMsg msg) {
        messageSender.println(msg.toJson());
        messageSender.flush();
    }
	
	
			// calculate velocity of specific car
	public void calculateVelocity(Car car) {
		double dx;
    	double dt = tick - previousTick;
    	
    	if(car.previousPosition.piecePosition.pieceIndex == car.position.piecePosition.pieceIndex) {			// positions on same piece
    		dx = car.position.piecePosition.inPieceDistance - car.previousPosition.piecePosition.inPieceDistance;
    	} else {																		
    		Piece piece = track.pieces.get(car.previousPosition.piecePosition.pieceIndex);			
			double laneDistanceFromCenter = track.lanes.get(car.position.piecePosition.lane.startLaneIndex).distanceFromCenter;
    		if(Math.abs(piece.angle) > 0) {																		// in case of curve
    			dx = (calculateLaneLength(piece, laneDistanceFromCenter) + 
    					car.position.piecePosition.inPieceDistance) - 
    					car.previousPosition.piecePosition.inPieceDistance;
    		} else {																							// straight piece
    			dx = (piece.length + car.position.piecePosition.inPieceDistance) - 
    					car.previousPosition.piecePosition.inPieceDistance;
    		}
    	}
    	
    	car.velocity = dx / dt;
	}
	
	double calculateLaneLength(Piece piece, double distanceFromCenter) {
		return Math.PI * (piece.radius - distanceFromCenter * Sector.sign(piece.angle)) * (Math.abs(piece.angle) / 180);
	}
	
	
				//	data parsing methods
	private void parsePositions(Object json) {
    	JsonParser parser = new JsonParser();
    	
    	JsonArray jArray = parser.parse(gson.toJson(json)).getAsJsonArray();
    	
    	List<CarPosition> positions = new ArrayList<CarPosition>();
    	
    	for(JsonElement jElement : jArray) {
    		CarPosition pos;
    		
    		pos = gson.fromJson(jElement, CarPosition.class);
    		
    		JsonElement id = jElement.getAsJsonObject().get("id");
    		JsonElement piecePosition = jElement.getAsJsonObject().get("piecePosition");
    		JsonElement lane = piecePosition.getAsJsonObject().get("lane");
    		
    		pos.id = gson.fromJson(id, position.Id.class);
    		pos.piecePosition = gson.fromJson(piecePosition, position.PiecePosition.class);
    		pos.piecePosition.lane = gson.fromJson(lane, position.Lane.class);
    		
    		positions.add(pos);
    	}
    	
    	for(CarPosition position : positions) {
    		if(position.id.color.equals(myCar.color)) {
    			me.updatePreviousPosition();
    			me.position = position;
    		} else {
//    			opponents.put(position.id.color, arg1)
    		}
    	}
    }
	
	private void parseRace(Object json) {
    	JsonParser parser = new JsonParser();
    	System.out.println(gson.toJson(json));
    	JsonElement element = parser.parse(gson.toJson(json));
    	JsonObject raceObject = element.getAsJsonObject().get("race").getAsJsonObject();;
//    	System.out.println(raceObject);
    	JsonObject trackObject = raceObject.get("track").getAsJsonObject();
    	parseTrack(trackObject);
    }
	
	private void parseTrack(JsonObject trackObject) {
//    	System.out.println(json);
//    	JsonParser parser = new JsonParser();

    	track = gson.fromJson(trackObject, Track.class);
    	
//    	JsonObject trackObject = parser.parse(json).getAsJsonObject();
    	
    	System.out.println(track.id);
    	System.out.println(track.name);
    	
    	Type listType = new TypeToken<List<Piece>>(){}.getType();
    	JsonElement pieces = trackObject.get("pieces");
    	track.pieces = gson.fromJson(pieces, listType);
    	
    	int i = 0;
    	
    	System.out.println(track.pieces.size());
    	for(Piece piece : track.pieces) {
    		System.out.print((i++) + ": " + piece.length + ", ");
    		System.out.print(piece.radius + ", ");
    		System.out.print(piece.angle + ", ");
    		System.out.println(piece.Switch);
    	}
    	
    	listType = new TypeToken<List<Lane>>(){}.getType();
    	JsonElement lanes = trackObject.get("lanes");
    	track.lanes = gson.fromJson(lanes, listType);
    	
    	for(Lane lane : track.lanes) {
    		System.out.print(lane.index + ", ");
    		System.out.println(lane.distanceFromCenter);
    	}
    	
    	JsonElement startingPoint = trackObject.get("startingPoint");
    	track.startingPoint = gson.fromJson(startingPoint, StartingPoint.class);
    	
    	JsonElement position = startingPoint.getAsJsonObject().get("position");
    	track.startingPoint.position = gson.fromJson(position, Position.class);
    	
    	System.out.println(track.startingPoint.position.x + ", " + track.startingPoint.position.y + ", " + track.startingPoint.angle);
    }
}
