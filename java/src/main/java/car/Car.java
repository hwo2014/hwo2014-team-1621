package car;

import position.CarPosition;

public class Car {
	public String id;
	public String name;
	public double throttle;
	
	public CarPosition position;
	public CarPosition previousPosition;
	
	public double velocity;
	public double previousVelocity;
	public double acceleration;
	public double k = -1;					// coefficient of velocity curve (with this we can calculate when to start decelerating)
	public double maxCornerVelocity = 5;
	
	public int sector = 0;


	public Car() {
		position = new CarPosition();
		previousPosition = new CarPosition();
	}
	
	
	public void updatePreviousPosition() {
		previousPosition.angle = position.angle;
		previousPosition.piecePosition.inPieceDistance = position.piecePosition.inPieceDistance;
		previousPosition.piecePosition.pieceIndex = position.piecePosition.pieceIndex;
		previousPosition.piecePosition.lap = position.piecePosition.lap;
		previousPosition.piecePosition.lane.startLaneIndex = position.piecePosition.lane.startLaneIndex;
		previousPosition.piecePosition.lane.endLaneIndex = position.piecePosition.lane.endLaneIndex;
	}
	
	
	
	public double koefficient(double vel, double throt) {
    	double vt = 10 * throt;
    	
    	k = (Math.log((vt) / (vt - vel)));
    	
    	return  k; // / (tick - previousTick);
    }
}
