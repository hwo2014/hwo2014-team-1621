package track;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Sector {
	public int index;
	public List<Lane> lanes;
	public List<Piece> pieces;
	public List<Double> laneLength;
	
	public Sector(int index, List<Lane> lanes) {
		this.index = index;
		this.lanes = lanes;
		
		pieces = new ArrayList<Piece>();
		laneLength = new ArrayList<Double>();
		
	}
	
	public void calculateLaneLengths() {
		double[] lengths = new double[lanes.size()];
		
		for(Lane lane : lanes) {
			for(Piece piece : pieces) {
				if(piece.radius == 0) {
					lengths[lane.index] += piece.length;
				} else {
					lengths[lane.index] += Math.PI * (piece.radius - lane.distanceFromCenter * sign(piece.angle)) * (Math.abs(piece.angle) / 180);
				}
			}
		}
		
		for(int i = 0; i < lengths.length; i++) {
			laneLength.add(lengths[i]);
		}
	}
	
	public int shortestLane() {
		System.out.println("lane lengths (sector " + index + "): ");
		for(int i = 0; i < laneLength.size(); i++) {
			System.out.println(laneLength.get(i));
		}
		
		if(Math.abs(laneLength.get(0) - laneLength.get(laneLength.size()-1)) < 0.1) {			// if straight, returns -1
			return -1;
		}
		
		System.out.println(index + ": " + laneLength.indexOf(Collections.min(laneLength)));
		
		return laneLength.indexOf(Collections.min(laneLength));
	}
	
	public static double sign(double value) {				// returns sign of the value (-1 or 1) to use in curves lane length calculation
		return value / Math.abs(value);
	}
}
