package track;

import java.util.ArrayList;
import java.util.List;

public class Track {
	public String id;
	public String name;
	public List<Piece> pieces;
	public List<Lane> lanes;
	public StartingPoint startingPoint;
	
	public Sector[] sectors;
	public ArrayList<Integer> switches;
	public int[] shortestRoute;
	
	public void buildSectors() {
		switches = switches();
		
		sectors = new Sector[switches.size()];
		
		for(int i = 0; i < sectors.length; i++) {
			sectors[i] = new Sector(i, lanes);
		}
		
		int sectorIndex = 0;
		
		for(int i = 0; i < pieces.size(); i++) {
			if(i > switches.get(sectorIndex)) {	// if switch is passed, sector changes
				sectorIndex++;														// and not last switch passed
			}
			
			if(i > switches.get(switches.size() - 1))						// pieces after last switch belong to first sector
				sectorIndex = 0;
			
			if(i != switches.get(sectorIndex)) {						// switches don't belong to sectors, and they aren't added
				sectors[sectorIndex].pieces.add(pieces.get(i));			// Adding references of pieces to correct sector's piece list
//				System.out.println("piece " + i + " added to sector " + sectorIndex);
			}
		}
		
		for(Sector sector : sectors) {
			sector.calculateLaneLengths();
		}
		
		findShortestRoute();
		
		for(int i = 0; i < sectors.length; i++) {
			for(int j = 0; j < sectors[i].pieces.size(); j++) {
				System.out.print(i + ": ");
				System.out.println(sectors[i].pieces.get(j));
			}
		}
	}
	
	public void findShortestRoute() {
		shortestRoute = new int[sectors.length];
		
		for(int i = 0; i < shortestRoute.length; i++) {
			shortestRoute[i] = sectors[i].shortestLane();
		}
		
//		for(int i = 0; i < shortestRoute.length; i++) {
//			if(lanes.size() == 2) {
//				if(shortestRoute[i] == -1) {
//					shortestRoute[i] = shortestRoute[nextSector(i)];
//				}
//			} else if(lanes.size() == 3) {
//				// must check 2 pieces forward, because changing 2 lanes takes 2 switches
//				// and even though changing 
//			} else {	// 4 lanes
//				// and here 3 pieces forward
//			}
//		}
		
		System.out.println("shortest route");
		for(int i = 0; i < shortestRoute.length; i++) {
			System.out.println("sector " + i + " lane " + shortestRoute[i]);
		}
	}
	
	// helper functions to get wanted sectors
	public int previousSector(int index) {
		if(index == 0) {
			return sectors.length - 1;
		}
		
		return index - 1;
	}
	
	public int nextSector(int index) {
		if(index == sectors.length - 1)
			return 0;
		
		return index + 1;
	}
	
	private ArrayList<Integer> switches() {									// Finds indexes of all switch pieces
		ArrayList<Integer> switches = new ArrayList<Integer>();
		
		for(int i = 0; i < pieces.size(); i++) {
			if(pieces.get(i).Switch)
				switches.add(i);
		}
		
		return switches;
	}
}