package track;

import com.google.gson.annotations.SerializedName;

public class Piece {
	public double length = 0;
	public double radius = 0;
	public double angle = 0;
	
	@SerializedName("switch")
	public boolean Switch = false;
	
	@Override
	public String toString() {
		return length + "," + radius + "," + angle + "," + Switch;
	}
}