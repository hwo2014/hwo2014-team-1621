package messages;

public class Response extends MsgWrapper {
    public String gameId = "";
    public int gameTick = -1;
    
    public Response(final String msgType, final Object data) {
    	super(msgType, data);
    }

}