package messages;

public class Turbo extends SendMsg {
	@Override
	protected String msgType() {
		return "turbo";
	}
	
	@Override
	protected Object msgData() {
		return "Grroaaa-- Clank clank! Wobblewobblewobble! Screeeeeeech!";
	}
}