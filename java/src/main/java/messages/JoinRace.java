package messages;

public class JoinRace extends SendMsg {
	public Data data;
	
	public JoinRace(String name, String key, String trackName, String password, int carCount) {
		data = new Data();
		data.botId = new BotId();
		
		data.trackName = trackName;
		data.password = password;
		data.carCount = carCount;
		
		data.botId.name = name;
		data.botId.key = key;
	}
	
	public JoinRace(String name, String key, String trackName) {
		this(name, key, trackName, null, 1);
	}
	
	@Override
	protected Object msgData() {
		return data;
	}
	
	@Override
	protected String msgType() {
		return "joinRace";
	}
	
	class Data {
		public BotId botId;
		public String trackName;
		public String password = null;
		public int carCount = 1;
	}

	class BotId {
		public String name;
		public String key;
	}
}