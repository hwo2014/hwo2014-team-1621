package messages;

public class SwitchLane extends SendMsg {
	private String lane;
	
	public SwitchLane(String to) {
		this.lane = to;
	}
	
	@Override
	protected Object msgData() {
		return lane;
	}
	
	@Override
	protected String msgType() {
		return "switchLane";
	}
}