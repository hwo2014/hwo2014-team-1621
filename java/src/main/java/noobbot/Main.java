// I ran out of time because I started a new job at the same week this competition started.



package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import evilbot.EvilBot;


public class Main {
	boolean log = true;
	EvilBot bot;
	
	static int run = 0;
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        Socket socket;
        PrintWriter writer; 

        BufferedReader reader; 


    	socket = new Socket(host, port);
    	writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
    	reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
    	new Main(reader, writer, botName, botKey);
    	
    	socket.close();
        
        
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey) throws IOException {
        bot = new EvilBot(reader, writer, botName, botKey);
        bot.run();
    }
}