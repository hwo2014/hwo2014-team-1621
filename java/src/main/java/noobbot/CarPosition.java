package noobbot;

class CarPosition {
	public String name = "";					// id data
	public String color = "";
	
	public double angle = 0;					// angle
	
	public double pieceIndex = 0;				// location on track
	public double inPieceDistance = 0;
		
	public double startLaneIndex = 0;			// lane info
	public double endLaneIndex = 0;
		
	public double lap = 0;						// which lap
	
	
	public void setId(String name, String color) {
		this.name = name;
		this.color = color;
	}
	
	public void setAngle(double angle) {
		this.angle = angle;
	}

	public void setPiecePosition(double pieceIndex, double inPieceDistance) {
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
	}
	
	public void setLane(double startLaneIndex, double endLaneIndex) {
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}
	
	public void setLap(double lap) {
		this.lap = lap;
	}
}